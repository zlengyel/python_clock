import turtle
import datetime
#print "Hello world"

myPen = turtle.Turtle()
myPen.shape("square")
myPen.tracer(0)
myPen.speed(0)

currentMinute = datetime.datetime.now().minute
currentHour = datetime.datetime.now().hour

#currentHour=11
#currentMinute = 57

myPen.penup()
myPen.goto(0,-180)
myPen.pensize(40)
myPen.pendown()
myPen.color("blue")
myPen.circle(180)

myPen.penup()
myPen.goto(0,-180)
myPen.shape("circle")
myPen.color("black")
myPen.pensize(20)
myPen.setheading(90)


myPen.color("black")
for x in range(0,4):
	myPen.penup()
	if x == 0:
		#myPen.color("white")
		myPen.goto(0,-180) #6
		myPen.setheading(90)
		#myPen.forward(10)
	elif x == 1:
		#myPen.color("brown")
		myPen.goto(-180,0) #9
		myPen.setheading(180)
	elif x == 2:
		#myPen.color("yellow")
		myPen.goto(0,180) #12
		myPen.setheading(90)
	else:
		#myPen.color("red")
		myPen.goto(180,0)
		myPen.setheading(180)
	myPen.pendown()
	myPen.forward(10)
	myPen.penup()


myPen.penup()
myPen.pensize(15)
myPen.color("red")
myPen.goto(0,0)
myPen.setheading(90)
#calculate angle form minutes, added to hour's angle
oneMinuteAngle = (360/60)
#print  oneMinuteAngle
myPen.right((currentHour*360/12) + oneMinuteAngle)
myPen.pendown()
myPen.forward(100)

myPen.penup()
myPen.goto(0,0)
myPen.pensize(10)
myPen.setheading(90)
myPen.right(currentMinute*360/60)
myPen.pendown()
myPen.forward(150)

myPen.penup()
myPen.goto(0,0)
myPen.color("yellow")
myPen.setheading(90)
myPen.pendown()
#myPen.forward(120)

def refreshClock():
	currentSecond = datetime.datetime.now().second



myPen.getscreen().update()

